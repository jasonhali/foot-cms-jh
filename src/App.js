import React, { Fragment, useEffect } from 'react'
import logo from './logo.svg'
import './App.css'
import ItemList from './Components/ItemList'
import Header from './Components/Home/Header'
import Body from './Components/Home/Body'
import { Routes, Route, Link, BrowserRouter } from 'react-router-dom'

import 'react-day-picker/lib/style.css'
// import 'react-day-picker/lib/style.css'

import GlobalContext, { WithContext } from './Components/Context'
import Modal from './Components/Modal/Modal'
export default class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      campaigns: [],
      displayModal: false,
      target:false
    }
  }

  triggerModal = () => {
    this.setState({ displayModal: !this.state.displayModal })
  }

  render () {
    return (
      <GlobalContext.Provider
        value={{
          triggerModal: this.triggerModal, displayModal: this.state.displayModal
        }}
      >

        <div className='App'>
          <BrowserRouter>
            <Routes>

              <Route
                path='/'
                element={
                  <>
                    <Modal 
                    triggerModal={this.triggerModal} 
                    displayModal= {this.state.displayModal}
                    />
                    <Header />
                    <Body />
                  </>
              }

              />

              <Route path='about' element={<Body />} />
            </Routes>
          </BrowserRouter>

          {/* //testing */}
          {/* <ItemList /> */}
        </div>
      </GlobalContext.Provider>
    )
  }
}
