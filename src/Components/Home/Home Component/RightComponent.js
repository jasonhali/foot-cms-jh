import React, { Fragment } from 'react'
import _ from 'lodash'

export default class RightComponent extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }

  render () {
    return (
      <div className='flex center h-full w-full'>
        <div className='Header w-ex-small main-tab-left main-tab-left-radius h-full border-right'>left</div>
        <div className='w-full main-tab-right h-full'>right</div>
      </div>
    )
  }
}
