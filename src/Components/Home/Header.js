import React, { useEffect, useState } from 'react'
import HomeIcon from '@mui/icons-material/Home'
import moment from 'moment'
// import { HomeIcon } from '@mui/icons-material'
import Search from '@mui/icons-material/Search'

function Header () {
  const [count, setCount] = useState(moment().format('MMMM Do YYYY, h:mm:ss a'))
  let a
  function name (params) {
    a = moment().format('MMMM Do YYYY, h:mm:ss a')
    setCount(a)
  }

  setInterval(function () {
    name()
  }, 1000)

  return (
    <div className='Header h-tiny'>

      <div className='Header w-tiny f-start'>
        <img src={require('../../Asset/main-logo.png')} height={90} style={{ paddingLeft: '10%' }} alt='Italian Trulli' />
      </div>

      <div className='Header w-medium h-full'>
        <input className='w-full h-half rad-tiny' id='homePage' />
        <Search className='absolute' style={{ left: '16.5%', color: '#3C4055' }} />

      </div>

      <div className='w-m-small'>
        <div style={{ color: 'white' }}>
          {count}
        </div>
      </div>

      <div className='Header w-ex-small f-center'>
        <div className='w-half d-inline-f'>
          Welcome, Admin
        </div>
        <div className='d-inline-f w-half'>
          <img src={require('../../Asset/dog.jpg')} height={60} width={60} alt='Italian Trulli' style={{ borderRadius: 315 }} />
        </div>
      </div>
    </div>
  )
}

// 15 - 40 - 20 - 25
// 55 + 45

export default Header
