import React, { Fragment } from 'react'
import _ from 'lodash'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import 'react-day-picker/lib/style.css'
import { formatDate, parseDate } from 'react-day-picker/moment'

export default class Modal extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      campaigns: [],
      displayModal: false,
      inputField: [
        { name: 'Name' },
        { name: 'Title' },
        { name: 'Url' }
      ],
      startDate: new Date()
    }
  }

  renderInputField (data, idx) {
    return (
      <div id='m-field' style={{ justifyContent: idx === 0 ? 'end' : idx === 1 ? 'center' : 'start' }}>
        <div>{`Content ${data.name}`}</div>
        <input id='m-input' placeholder={`Input Content ${data.name}`} />
      </div>
    )
  }

  renderDatePick () {
    const { startDate } = this.state
    return (
      <div className='h-full'>
        <div className='w-full h-half column center' id='picker'>
          <div>Start Date</div>
          <DayPickerInput
            formatDate={formatDate}
            parseDate={parseDate}
            value={startDate}
            onDayChange={day => this.setState({ startDate: day })}
            dayPickerProps={{
              disabledDays: { before: new Date() }
            }}
          />
        </div>

        <div className='w-full h-half column center' id='picker'>
          <div>End Date</div>
          <DayPickerInput
            formatDate={formatDate}
            parseDate={parseDate}
            value={startDate}
            onDayChange={day => this.setState({ startDate: day })}
            dayPickerProps={{
              disabledDays: { before: new Date() }
            }}
          />
        </div>
      </div>
    )
  }

  render () {
    const { displayModal } = this.props
    const { inputField, startDate } = this.state
    console.log('🚀 ~ file: Modal.js ~ line 44 ~ Modal ~ render ~ startDate', startDate)
    if (displayModal) {
      return (
        <div className='flex h-full w-full absolute center' id='modal'>
          <div className='column center h-half w-medium modal absolute center'>
            <div id='modal-header' classNaeme='column'>
              <div id='modal-title' className='w-full center flex'>ADD ITEM</div>
              <hr id='m-hr' />
            </div>

            <div id='modal-body' className='w-full flex'>
              <div className='h-full flex center' id='m-content-left'>
                {inputField.map((data, idx) => (
                  <Fragment key={idx}>
                    {this.renderInputField(data, idx)}
                  </Fragment>
                ))}
              </div>

              <div className='h-full w-half' id='m-content-right'>
                {this.renderDatePick()}
              </div>
            </div>

            <div id='modal-footer' className='w-full flex center'>
              <button>SUBMIT</button>
            </div>

          </div>
        </div>
      )
    } else {
      return null
    }
  }
}
